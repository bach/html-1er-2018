// console.log("Hello");

// variable de texte / chaine de characteres (string)
var titre = "Mon super jeu";
// console.log(titre);

// variable de nombre
var score = 34;
// console.log(score);

// addition
var score2 = score + 3;
console.log(score2);

// concatenation
var titre2 = titre + " trop top !";
console.log(titre2);

// piege
var a = 2;
var b = "3";
var c = a + b;
console.log(c);

// tableau (array)
var malistedecourse = ['salade', "beure", "toffu", "lait", "pate", "fromage rapé", "sauce tomate"];
console.log(malistedecourse);

var cesoir = malistedecourse[4];
console.log(cesoir);

// la variable document exist deja par defaut
console.log(document);

// html dom
// var divlist = document.querySelectorAll('div');
// console.log(divlist);
var monperso = document.querySelector('.personnage');
console.log(monperso);

// monperso.style.backgroundColor = "green";
monperso.style.top = "130px";
monperso.style.left = "45px";

var vitesse = 3;
var pos_gauche_perso = 0;
var animate = function(){
  // avance vers la droite
  // si avance vers la droite et bord droit alors avance vers la gauche
  // si avance vers gauche et bord gauche alors avance vers la droite
  if(vitesse > 0 && pos_gauche_perso > 400){
    vitesse = -3;
  }else if(vitesse < 0 && pos_gauche_perso < 0){
    vitesse = 3;
  }

  if(vitesse > 0){
    monperso.classList.add("gd");
    monperso.classList.remove("dg");
  }else{
    monperso.classList.add("dg");
    monperso.classList.remove("gd");
  }

  pos_gauche_perso = pos_gauche_perso + vitesse;
  monperso.style.left = pos_gauche_perso+'px';

  // if(pos_gauche_perso < 400){
  //   pos_gauche_perso++; // pos_gauche_perso = pos_gauche_perso + 1;
  //   monperso.style.left = pos_gauche_perso+'px';
  // }else{
  //   pos_gauche_perso = 0;
  // }

  window.requestAnimationFrame(animate);

  //
  // if (true) {
  //   // tu fais qlq chose
  // }else if (true) {
  //   // tu fais qlq chose
  // }else if (true) {
  //   // tu fais qlq chose
  // }else {
  //   // tu fais qlq chose
  // }


}
animate();


// Les functions
var addition = function(a,b){
  return a+b;
}

var f = 3;
var k = 56;
var c = addition(f,k);
console.log(c);

var d = addition(45,78);
console.log(d);

var e = addition(12,7);
console.log(e);

// Evenements (events)
document.addEventListener('click', onClickDoc);

function onClickDoc(event){
  console.log("j'ai cliqué sur le document");
}

document.addEventListener('keyup', onKeyupDoc);

function onKeyupDoc(event){
  console.log('on key up document', event);

  // if (event.key == "d") {
  //   console.log('Salut');
  // }else if(event.key == "s"){
  //   console.log('Youpi');
  // }

  switch (event.key) {
    case "d":
      console.log("D");
      break;
    case "z":
      console.log("Z");
      break;
    case "ArrowUp":
      console.log("up");
      break;
    case " ":
      console.log("space");
      break;
  }


}

var canard = document.querySelector('.canard');

canard.addEventListener('click', onClickCanard);

function onClickCanard(event){
  console.log('on click canard');
}
